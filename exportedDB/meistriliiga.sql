-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-06-2019 a las 23:09:03
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `meistriliiga`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `service` varchar(100) NOT NULL COMMENT 'Nombre del servicio',
  `email` varchar(150) NOT NULL COMMENT 'Contacto del servicio',
  `token` varchar(255) NOT NULL COMMENT 'Token del servicio'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth`
--

INSERT INTO `auth` (`id`, `service`, `email`, `token`) VALUES
(1, 'meistriliiga', 'info@meistriliiga.com', '7DbevHzBhayDrP4GBN0G6H1qw7GU54YJnQor2Yr7iSubyB7jNSf2HO1bq1zPxyc9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL COMMENT 'Id del partido al que pertenete el evento',
  `type` varchar(50) NOT NULL COMMENT 'Tipo de evento',
  `players` varchar(100) NOT NULL COMMENT 'Jugador o jugadores involucrados',
  `minute` int(11) NOT NULL COMMENT 'Minuto del evento'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `match_id`, `type`, `players`, `minute`) VALUES
(1, 1, 'goal', 'Sorga Erik', 37),
(2, 1, 'yellow card', 'Vallner Karl Andre', 60);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `home` varchar(100) NOT NULL COMMENT 'Equipo que juega en casa',
  `visitor` varchar(100) NOT NULL COMMENT 'Equipo que juega como visitante',
  `result` varchar(10) NOT NULL COMMENT 'Resultado del partido',
  `home_lineup` longtext DEFAULT NULL COMMENT 'Once inicial del equipo de casa',
  `visitor_lineup` longtext DEFAULT NULL COMMENT 'Once inicial del equipo que juega como visitante',
  `location` varchar(100) NOT NULL COMMENT 'Ciudad en la que se juega',
  `time` int(20) NOT NULL COMMENT 'Timestamp de comienzo del partido',
  `status` int(11) NOT NULL COMMENT 'Status del partido'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `matches`
--

INSERT INTO `matches` (`id`, `home`, `visitor`, `result`, `home_lineup`, `visitor_lineup`, `location`, `time`, `status`) VALUES
(1, 'Tallinna Kalev', 'FC Flora', '0 - 1', '[\"Vallner Karl Andre\",\"Kouyat\\u00e9 Bangaly Theodor\",\"Lee Roger\",\"Allast Markus\",\"Ostrovski Artjom\",\"Maksimenko Denis\",\"Raudsepp Andreas\",\"Sheviakov Daniil\",\"Bachmann Brandon\",\"Anier Hannes\",\"Raabis Kevin\"]', '[\"Aland Richard\",\"Kams Gert\",\"Kuusk Marten\",\"Purg Henrik\",\"Riiberg Herol\",\"Kreida Vladislav\",\"Miller Martin\",\"Sinyavskiy Vlasiy\",\"Vassiljev Konstantin\",\"Liivak Frank\",\"Sorga Erik\"]', 'Tallin', 1561919100, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
