<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190623210444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE auth CHANGE service service VARCHAR(100) NOT NULL, CHANGE email email VARCHAR(150) NOT NULL, CHANGE token token VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE events CHANGE match_id match_id INT NOT NULL, CHANGE type type VARCHAR(50) NOT NULL, CHANGE players players VARCHAR(100) NOT NULL, CHANGE minute minute INT NOT NULL');
        $this->addSql('ALTER TABLE matches CHANGE home home VARCHAR(100) NOT NULL, CHANGE visitor visitor VARCHAR(100) NOT NULL, CHANGE result result VARCHAR(10) NOT NULL, CHANGE home_lineup home_lineup LONGTEXT DEFAULT NULL, CHANGE visitor_lineup visitor_lineup LONGTEXT DEFAULT NULL, CHANGE location location VARCHAR(100) NOT NULL, CHANGE time time INT NOT NULL, CHANGE status status INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE auth CHANGE service service VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT \'Nombre del servicio\', CHANGE email email VARCHAR(150) NOT NULL COLLATE utf8_general_ci COMMENT \'Contacto del servicio\', CHANGE token token VARCHAR(255) NOT NULL COLLATE utf8_general_ci COMMENT \'Token del servicio\'');
        $this->addSql('ALTER TABLE events CHANGE match_id match_id INT NOT NULL COMMENT \'Id del partido al que pertenete el evento\', CHANGE type type VARCHAR(50) NOT NULL COLLATE utf8_general_ci COMMENT \'Tipo de evento\', CHANGE players players VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT \'Jugador o jugadores involucrados\', CHANGE minute minute INT NOT NULL COMMENT \'Minuto del evento\'');
        $this->addSql('ALTER TABLE matches CHANGE home home VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT \'Equipo que juega en casa\', CHANGE visitor visitor VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT \'Equipo que juega como visitante\', CHANGE result result VARCHAR(10) NOT NULL COLLATE utf8_general_ci COMMENT \'Resultado del partido\', CHANGE home_lineup home_lineup LONGTEXT DEFAULT NULL COLLATE utf8_general_ci COMMENT \'Once inicial del equipo de casa\', CHANGE visitor_lineup visitor_lineup LONGTEXT DEFAULT NULL COLLATE utf8_general_ci COMMENT \'Once inicial del equipo que juega como visitante\', CHANGE location location VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT \'Ciudad en la que se juega\', CHANGE time time INT NOT NULL COMMENT \'Timestamp de comienzo del partido\', CHANGE status status INT NOT NULL COMMENT \'Status del partido\'');
    }
}
