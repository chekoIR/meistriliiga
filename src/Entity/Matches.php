<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchesRepository")
 */
class Matches
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $home;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $visitor;

     /**
     * @ORM\Column(type="string", length=10)
     */
    private $result;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $homeLineup;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $visitorLineup;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $location;

    /**
     * @ORM\Column(type="integer")
     */
    private $time;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHome(): ?string
    {
        return $this->home;
    }

    public function setHome(string $home): self
    {
        $this->home = $home;

        return $this;
    }

    public function getVisitor(): ?string
    {
        return $this->visitor;
    }

    public function setVisitor(string $visitor): self
    {
        $this->visitor = $visitor;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getHomeLineup(): ?string
    {
        return $this->homeLineup;
    }

    public function setHomeLineup(string $homeLineup): self
    {
        $this->homeLineup = $homeLineup;

        return $this;
    }

    public function getVisitorLineup(): ?string
    {
        return $this->visitorLineup;
    }

    public function setVisitorLineup(string $visitorLineup): self
    {
        $this->visitorLineup = $visitorLineup;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
