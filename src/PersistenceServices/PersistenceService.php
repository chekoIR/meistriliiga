<?php

namespace App\PersistenceServices;

#use App\PersistenceService\IPersistenceService;
use Doctrine\ORM\EntityManagerInterface;

abstract class PersistenceService /*implements IPersistenceService*/ {
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    protected function entityFactory($type) {
        $obj = new $type();
        return $this->entityManager->merge($obj);
        //return $obj;
    }

    protected function persist($object) {
        $this->entityManager->persist($object);
        $this->entityManager->flush();

        return $this;
    }
    
    protected function find($id) {
        return $this->findOneByCriteria(['id' => $id]);
    }

    public function getRepositoryName() {
        return static::REPOSITORY_NAME;
    }

    public function save($object) {
        return $this->persist($object);
    }

    public function delete($object) {

        $this->entityManager->remove($object);
        $this->entityManager->flush();
    }

    public function findOneByCriteria($criteria) {
        /* @var $repository \Doctrine\ORM\EntityRepository */
        $repository = $this->getRepository();
        $object = $repository->findOneBy($criteria);

        return !is_object($object) ? null : $object;
    }

    public function findAllByCriteria($criteria) {
        /* @var $repository \Doctrine\ORM\EntityRepository */
        $repository = $this->getRepository();
        return $repository->findBy($criteria);
    }

    
    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository() {
        return $this->entityManager->getRepository($this->getRepositoryName());
    }
   

}
