<?php

namespace App\PersistenceServices;

use App\PersistenceServices\PersistenceService;
use App\PersistenceServices\IPersistenceService;
use App\Entity\Matches;
use \Exception;

class MatchesService extends PersistenceService implements IPersistenceService 
{
    const REPOSITORY_NAME = 'App:Matches';
    const ID = 'id';

    public function remove($object) {
        return parent::delete($object);
    }

    public function getRepositoryName() {
        return self::REPOSITORY_NAME;
    }

    public function save($object) {
        try {
            return parent::save($object);
        } catch (Exception $e) {
            return false;
        }
    }

    public function getById($id)
    {
        return $this->findOneByCriteria(["id"=>$id]);
    }

    public function findOneByCriteria($criteria = [])
    {
        return parent::findOneByCriteria($criteria);
    }

    public function findAllByCriteria($criteria)
    {
        return parent::findAllByCriteria($criteria);
    }

    public function create($data) 
    {
        $matchesEntity = new Matches();

        //Home
        if((array_key_exists('home', $data))  && (isset($data['home']))){
           $matchesEntity->setHome($data['home']); 
        }
        else{
            throw new \Exception('home param is required to create a matches Entity');
        }

        //Visitor
        if((array_key_exists('visitor', $data))  && (isset($data['visitor']))){
            $matchesEntity->setVisitor($data['visitor']);
        }
        else{
            throw new \Exception('visitor param is required to create a matches Entity');
        }

        //homelineup
        if((array_key_exists('homeLineup', $data))  && (isset($data['homeLineup']))){
            $matchesEntity->setHomeLineup($data['homeLineup']);
        }
        else{
            $matchesEntity->setHomeLineup('');
        }

        //visitorLineup
        if((array_key_exists('visitorLineup', $data))  && (isset($data['visitorLineup']))){
            $matchesEntity->setVisitorLineup($data['visitorLineup']);
        }
        else{
            $matchesEntity->setVisitorLineup('');
        }  

        //location
        if((array_key_exists('location', $data))  && (isset($data['location']))){
            $matchesEntity->setLocation($data['location']);
        }
        else{
            throw new \Exception('location param is required to create a matches Entity');
        }  

        //time 
        if((array_key_exists('time', $data))  && (isset($data['time']))){
            $matchesEntity->setTime($data['time']);
        }
        else{
            throw new \Exception('time param is required to create a matches Entity');
        } 

        //status
        if((array_key_exists('status', $data))  && (isset($data['status']))){
            $matchesEntity->setStatus((int) $data['status']);
        }
        else{
            throw new \Exception('status param is required to create a matches Entity');
        }

        //result 
        if((array_key_exists('result', $data))  && (isset($data['result']))){
            $matchesEntity->setResult($data['result']);
        }
        else{
            throw new \Exception('result param is required to create a matches Entity');
        }

        return $matchesEntity;

    }

}
