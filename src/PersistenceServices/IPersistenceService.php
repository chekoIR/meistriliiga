<?php

namespace App\PersistenceServices;
/*
 * Interface IPersistenceService
 * @package App\PersistenceService
 *
 */
interface IPersistenceService
{
    /*
     * Save a doctrine object into database
     * @param $object
     * @return mixed
     */
    public function save($object);

    /*
    * Get an object from the repository by id
    * @param $objectId
    * @return mixed
    */
    public function getById($objectId);

    /*
     * Get the name of the repository the service is working with
     * @return string
     */
    public function getRepositoryName();


    public function remove($object);
    
    public function findOneByCriteria($criteria);
    
    public function findAllByCriteria($criteria);

    /**
     * Create a entity class from array or stdClass
     * @param array|\stdClass $data
     */
    public function create($data);
}
