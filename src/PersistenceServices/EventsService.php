<?php

namespace App\PersistenceServices;

use App\PersistenceServices\PersistenceService;
use App\PersistenceServices\IPersistenceService;
use App\Entity\Events;

class EventsService extends PersistenceService implements IPersistenceService 
{
    const REPOSITORY_NAME = 'App:Events';
    const ID = 'id';

    public function remove($object) {
        return parent::delete($object);
    }

    public function getRepositoryName() {
        return self::REPOSITORY_NAME;
    }

    public function save($object) {
        try {
            return parent::save($object);
        } catch (Exception $e) {
            return false;
        }
    }

    public function getById($id)
    {
        return $this->findOneByCriteria(["id"=>$id]);
    }

    public function findOneByCriteria($criteria = [])
    {
        return parent::findOneByCriteria($criteria);
    }

    public function findAllByCriteria($criteria)
    {
        return parent::findAllByCriteria($criteria);
    }

    public function create($data) 
    {
        $Events = new Events();

        //elemento
        if((array_key_exists('match_id', $data))  && (isset($data['match_id']))){
           $Events->setMatchId($data['match_id']); 
        }
        else{
            throw new \Exception('match_id param is required to create an event Entity');
        }

        //elemento
        if((array_key_exists('type', $data))  && (isset($data['type']))){
           $Events->setType($data['type']); 
        }
        else{
            throw new \Exception('type param is required to create an event Entity');
        }

        //elemento
        if((array_key_exists('players', $data))  && (isset($data['players']))){
           $Events->setPlayers($data['players']); 
        }
        else{
            throw new \Exception('players param is required to create an event Entity');
        }

        //elemento
        if((array_key_exists('minute', $data))  && (isset($data['minute']))){
           $Events->setMinute($data['minute']); 
        }
        else{
            throw new \Exception('minute param is required to create an event Entity');
        }

        return $Events;
    }

}
