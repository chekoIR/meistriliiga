<?php

namespace App\PersistenceServices;

use App\PersistenceServices\PersistenceService;
use App\PersistenceServices\IPersistenceService;
use App\Entity\Auth;

class LoginAuthService extends PersistenceService implements IPersistenceService {
    const REPOSITORY_NAME = 'App:Auth';
    const ID = 'id';

    public function remove($object) {
        return parent::delete($object);
    }

    public function getRepositoryName() {
        return self::REPOSITORY_NAME;
    }

    public function save($object) {
        try {
            return parent::save($object);
        } catch (Exception $e) {
            return false;
        }
    }

    public function getById($id)
    {
        return $this->findOneByCriteria(["id"=>$id]);
    }

    public function findOneByCriteria($criteria = [])
    {
        return parent::findOneByCriteria($criteria);
    }

    public function findAllByCriteria($criteria)
    {
        return parent::findAllByCriteria($criteria);
    }

    public function create($data) 
    {
        
    }

}
