<?php

namespace App\BusinessServices;

use App\PersistenceServices\MatchesService;
use App\PersistenceServices\EventsService;
use App\BusinessServices\BusinessInterfaces\IMatehcesStatus;
use Symfony\Component\HttpFoundation\Response;

class PayloadManager {
    private $eventsService;
    private $matchesService;

    public function __construct(MatchesService $matchesService, EventsService $eventsService){
        $this->eventsService = $eventsService;
        $this->matchesService = $matchesService;
    }

    public function managePayload($data){
    	if((in_array('scorer_name', array_keys($data))) && (count($data) > 4)){
    		$this->storeEventInfo($data);
    	}
    	else{
    		$this->storeMatchInfo($data);
    	}
    }

    private function storeEventInfo($data){
    	$teams = explode(' - ', $data['teams']);
    	$home = $teams[0];
    	$visitor = $teams[1];

    	#actualizacion Match
    	$MatchesEntity = $this->matchesService->findOneByCriteria(['home'=>$home, 'visitor' => $visitor]);
    	$MatchesEntity->setHomeLineup(json_encode($data['players'][$home]));
    	$MatchesEntity->setVisitorLineup(json_encode($data['players'][$visitor]));
    	$MatchesEntity->setResult($data['result']);
    	$this->matchesService->save($MatchesEntity);

    	#generacion de registro en tabla eventos
    	$type = ($data["scorer_name"] == '') ? 'card' : 'goal';
    	if($type == 'card'){
    		$type = $data['card_player']['color'].' card';
    		$player = $data['card_player']['player'];
    		$minute = $data['card_minute'];
    	}
    	else{
    		$player = $data["scorer_name"];
    		$minute = $data["goal_minute"];
    	}


    	$dataEvent = [
    		"match_id" => $MatchesEntity->getId(),
    		"type" => $type,
    		"players" => $player,
    		"minute" =>$minute
    	];

    	$eventEntity = $this->eventsService->create($dataEvent);
    	$this->eventsService->save($eventEntity);
    }


    private function storeMatchInfo($data){
  		$teams = explode(' - ', $data['teams']);
    	$home = $teams[0];
    	$visitor = $teams[1];
		
		$MatchesEntity = $this->matchesService->findOneByCriteria(['home'=>$home, 'visitor' => $visitor]);
		if(($data['result'] == '')){
			$status = IMatehcesStatus::NOT_STARTED;
		}
		else{
			if((strtotime($data['datetime']) + 7200) > time()){
				$status = IMatehcesStatus::IN_PROCESS;
			}
			else{
				$status = IMatehcesStatus::FINISHED;
			}
		}

		if(is_null($MatchesEntity)){
			#nuevo partido
			$entityData = [
				"home" => $home,
				"visitor" => $visitor, 
				"location" => $data['location'],
				"result" => $data['result'],
				"time" => strtotime($data['datetime']),
				"status" => (int) $status,
				"result" => $data['result']
			];

			$MatchesEntity = $this->matchesService->create($entityData);

		} 
		else{
			#actualizacion de partido
			$MatchesEntity->setHome($home);
			$MatchesEntity->setVisitor($visitor);
			$MatchesEntity->setLocation($data['location']);
			$MatchesEntity->setTime(strtotime($data['datetime']));
			$MatchesEntity->setStatus((int) $status);
			$MatchesEntity->setResult($data['result']);
		}
		
		$this->matchesService->save($MatchesEntity);
    }
}

