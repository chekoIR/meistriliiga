<?php

namespace App\BusinessServices\BusinessInterfaces;

interface IMatehcesStatus {
    const NOT_STARTED = 1;
    const IN_PROCESS = 2;
    const FINISHED = 3;
}
