<?php

namespace App\BusinessServices;

use Symfony\Component\HttpFoundation\Response;

class SmsService {
    private $suscribersService;
    private $mobileNotificationsService;
    
    function __construct(/*$suscribersService, $mobileNotificationsService*/){
        //$this->suscribersService = $suscribersService;
        //$this->mobileNotificationsService = $mobileNotificationsService;
    }

    function notifyUsers($payload){
    	//Usamos el suscriberService para pedir a una hipotetica tabla de usuarios suscritos a nuestra web la lista completa de numeros de moviles a los que enviar la notificación

    	//Una vez tenemos el listado de usuarios usamos el mobileNotificationsService que hemos inyectado para hacer la llamada a la API de turno para realizar el envio de las notificaciones via SMS
    }
}