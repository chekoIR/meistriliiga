<?php

namespace App\Controller;

interface IController {
    public function indexAction(\Symfony\Component\HttpFoundation\Request $request);

}