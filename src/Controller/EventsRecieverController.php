<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\IController;
use App\BusinessServices\SmsService;
use App\BusinessServices\PayloadManager;
use App\PersistenceServices\LoginAuthService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Exception;


class EventsRecieverController extends AbstractController implements IController
{
    private $smsService;
    private $payloadManager;
    private $loginAuthService;

    function __construct(SmsService $smsService, PayloadManager $payloadManager, LoginAuthService $loginAuthService){
        $this->smsService = $smsService;
        $this->payloadManager = $payloadManager;
        $this->loginAuthService = $loginAuthService;
    }

    public function indexAction(Request $request)
    {
    	try {
    		$this->isAuthorized($request);
    		$payload = $request->get('payload');
            $payloadDecoded = json_decode($payload, true);

            if(empty($payloadDecoded)){
                throw new Exception("Event payload cannot be empty");
            }

            $this->payloadManager->managePayload($payloadDecoded);
            $this->smsService->notifyUsers($payload);

        	return new JsonResponse(['status' => Response::HTTP_OK, "message" => 'Event stored and notified']);
    	} 
        catch (Exception $e) {
    		return new JsonResponse(['status' => Response::HTTP_BAD_REQUEST, "message" => $e->getMessage()]);
    	}
    	
    }

    private function validateRequest(Request $request)
    {
        $auth = $request->server->get('HTTP_AUTHORIZATION');
        $token = trim(str_replace("Bearer","",$auth));
        $check = $this->loginAuthService->findOneByCriteria(['token' => $token]);
        return new Response(!is_null($check) ? "success" : "error");
    }

    private function isAuthorized(Request $request)
    {   
        if(self::validateRequest($request)->getContent() === "error") {
            throw new \Exception("Unauthorized", Response::HTTP_UNAUTHORIZED);
        }
    }
}
