CREATE TABLE `meistriliiga`.`matches` 
  ( 
     `id` INT(11) NOT NULL auto_increment, 
     `home` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL comment 'Equipo que juega en casa', 
     `visitor` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL comment 'Equipo que juega como visitante', 
     `homelineup`    LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL comment 'Once inicial del equipo de casa', 
     `visitorlineup` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL comment 'Once inicial del equipo que juega como visitante', 
     `location`      VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL comment 'Ciudad en la que se juega', 
     `time` INT(20) NOT NULL comment 'timestamp de comienzo del partido', 
     `status` INT NOT NULL comment 'Status en el que se encuentra el partido', 
     PRIMARY KEY (`id`) 
  ) 
engine = innodb; 


CREATE TABLE `meistriliiga`.`events` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `match_id` INT(11) NOT NULL COMMENT 'Id del partido al que pertenete el evento' , `type` VARCHAR(50) NOT NULL COMMENT 'Tipo de evento' , `players` VARCHAR(100) NOT NULL COMMENT 'Jugador o jugadores involucrados' , `minute` INT(11) NOT NULL COMMENT 'Minuto del evento' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `meistriliiga`.`auth` ( `id` INT NOT NULL AUTO_INCREMENT , `service` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del servicio' , `email` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Contacto del servicio' , `token` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Token del servicio' , PRIMARY KEY (`id`)) ENGINE = InnoDB;


testwallbox - a2YgRrnFCE9D7YnJ

GRANT ALL PRIVILEGES ON meistriliiga.* TO 'testwallbox'@'localhost'